#include <stdio.h>

#define NUM_ELEMENTS 7

int add(int v[],int tam);

int main()
{
    int vector[NUM_ELEMENTS] = { 2, 5, -2, 9, 12, -4, 3 };
    int result;

    result = add(vector, NUM_ELEMENTS);
    printf("The addition is: %d\n", result);
    return 0;
}

int add(int v[],int tam){
	int i;
	int suma = 0;
	for(i=0;i<tam;i++){
		suma=suma+v[i];
	}
	return suma;
}
