Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  ns/call  ns/call  name    
 28.85      0.30     0.30  4393752    68.28    68.28  mexp
 26.44      0.57     0.28  2151460   127.82   127.82  mlog
 15.38      0.73     0.16   788343   202.96   202.96  mfabs
  8.65      0.82     0.09   556113   161.84   161.84  mfloor
  6.73      0.90     0.07  1058593    66.13    66.13  msqrt
  3.85      0.94     0.04   327722   122.05   122.05  msin
  2.88      0.96     0.03   436563    68.72    68.72  mcos
  2.88      0.99     0.03                             thread_job
  1.92      1.01     0.02   466166    42.90    42.90  mceil
  1.92      1.03     0.02   219838    90.98    90.98  mcosh
  0.48      1.04     0.01   110072    45.42    45.42  msinh
  0.00      1.04     0.00   109902     0.00     0.00  get_random
  0.00      1.04     0.00        2     0.00     0.00  task

 %         the percentage of the total running time of the
time       program used by this function.

cumulative a running sum of the number of seconds accounted
 seconds   for by this function and those listed above it.

 self      the number of seconds accounted for by this
seconds    function alone.  This is the major sort for this
           listing.

calls      the number of times this function was invoked, if
           this function is profiled, else blank.

 self      the average number of milliseconds spent in this
ms/call    function per call, if this function is profiled,
	   else blank.

 total     the average number of milliseconds spent in this
ms/call    function and its descendents per call, if this
	   function is profiled, else blank.

name       the name of the function.  This is the minor sort
           for this listing. The index shows the location of
	   the function in the gprof listing. If the index is
	   in parenthesis it shows where it would appear in
	   the gprof listing if it were to be printed.

Copyright (C) 2012-2015 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

		     Call graph (explanation follows)


granularity: each sample hit covers 4 byte(s) for 0.96% of 1.04 seconds

index % time    self  children    called     name
                                                 <spontaneous>
[1]    100.0    0.03    1.01                 thread_job [1]
                0.30    0.00 4393752/4393752     mexp [2]
                0.28    0.00 2151460/2151460     mlog [3]
                0.16    0.00  788343/788343      mfabs [4]
                0.09    0.00  556113/556113      mfloor [5]
                0.07    0.00 1058593/1058593     msqrt [6]
                0.04    0.00  327722/327722      msin [7]
                0.03    0.00  436563/436563      mcos [8]
                0.02    0.00  466166/466166      mceil [9]
                0.02    0.00  219838/219838      mcosh [10]
                0.01    0.00  110072/110072      msinh [11]
-----------------------------------------------
                0.30    0.00 4393752/4393752     thread_job [1]
[2]     28.8    0.30    0.00 4393752         mexp [2]
-----------------------------------------------
                0.28    0.00 2151460/2151460     thread_job [1]
[3]     26.4    0.28    0.00 2151460         mlog [3]
-----------------------------------------------
                0.16    0.00  788343/788343      thread_job [1]
[4]     15.4    0.16    0.00  788343         mfabs [4]
-----------------------------------------------
                0.09    0.00  556113/556113      thread_job [1]
[5]      8.7    0.09    0.00  556113         mfloor [5]
-----------------------------------------------
                0.07    0.00 1058593/1058593     thread_job [1]
[6]      6.7    0.07    0.00 1058593         msqrt [6]
-----------------------------------------------
                0.04    0.00  327722/327722      thread_job [1]
[7]      3.8    0.04    0.00  327722         msin [7]
-----------------------------------------------
                0.03    0.00  436563/436563      thread_job [1]
[8]      2.9    0.03    0.00  436563         mcos [8]
-----------------------------------------------
                0.02    0.00  466166/466166      thread_job [1]
[9]      1.9    0.02    0.00  466166         mceil [9]
-----------------------------------------------
                0.02    0.00  219838/219838      thread_job [1]
[10]     1.9    0.02    0.00  219838         mcosh [10]
-----------------------------------------------
                0.01    0.00  110072/110072      thread_job [1]
[11]     0.5    0.01    0.00  110072         msinh [11]
-----------------------------------------------
                0.00    0.00  109902/109902      task [13]
[12]     0.0    0.00    0.00  109902         get_random [12]
-----------------------------------------------
                0.00    0.00       2/2           main [19]
[13]     0.0    0.00    0.00       2         task [13]
                0.00    0.00  109902/109902      get_random [12]
-----------------------------------------------

 This table describes the call tree of the program, and was sorted by
 the total amount of time spent in each function and its children.

 Each entry in this table consists of several lines.  The line with the
 index number at the left hand margin lists the current function.
 The lines above it list the functions that called this function,
 and the lines below it list the functions this one called.
 This line lists:
     index	A unique number given to each element of the table.
		Index numbers are sorted numerically.
		The index number is printed next to every function name so
		it is easier to look up where the function is in the table.

     % time	This is the percentage of the `total' time that was spent
		in this function and its children.  Note that due to
		different viewpoints, functions excluded by options, etc,
		these numbers will NOT add up to 100%.

     self	This is the total amount of time spent in this function.

     children	This is the total amount of time propagated into this
		function by its children.

     called	This is the number of times the function was called.
		If the function called itself recursively, the number
		only includes non-recursive calls, and is followed by
		a `+' and the number of recursive calls.

     name	The name of the current function.  The index number is
		printed after it.  If the function is a member of a
		cycle, the cycle number is printed between the
		function's name and the index number.


 For the function's parents, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the function into this parent.

     children	This is the amount of time that was propagated from
		the function's children into this parent.

     called	This is the number of times this parent called the
		function `/' the total number of times the function
		was called.  Recursive calls to the function are not
		included in the number after the `/'.

     name	This is the name of the parent.  The parent's index
		number is printed after it.  If the parent is a
		member of a cycle, the cycle number is printed between
		the name and the index number.

 If the parents of the function cannot be determined, the word
 `<spontaneous>' is printed in the `name' field, and all the other
 fields are blank.

 For the function's children, the fields have the following meanings:

     self	This is the amount of time that was propagated directly
		from the child into the function.

     children	This is the amount of time that was propagated from the
		child's children to the function.

     called	This is the number of times the function called
		this child `/' the total number of times the child
		was called.  Recursive calls by the child are not
		listed in the number after the `/'.

     name	This is the name of the child.  The child's index
		number is printed after it.  If the child is a
		member of a cycle, the cycle number is printed
		between the name and the index number.

 If there are any cycles (circles) in the call graph, there is an
 entry for the cycle-as-a-whole.  This entry shows who called the
 cycle (as parents) and the members of the cycle (as children.)
 The `+' recursive calls entry shows the number of function calls that
 were internal to the cycle, and the calls entry for each member shows,
 for that member, how many times it was called from other members of
 the cycle.

Copyright (C) 2012-2015 Free Software Foundation, Inc.

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.

Index by function name

  [12] get_random              [4] mfabs                   [6] msqrt
   [9] mceil                   [5] mfloor                 [13] task
   [8] mcos                    [3] mlog                    [1] thread_job
  [10] mcosh                   [7] msin
   [2] mexp                   [11] msinh
